export default {
    App : require('./lib/application'),
    Controller : require('./lib/controller'),
    Collection : require('./lib/collection')
}