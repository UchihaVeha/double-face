import React from 'react';
var ReactDOM = require('react-dom');
var ReactDOMServer = require('react-dom/server');
var _ = require('underscore');
var Navigation = require('./libs/navigation');
import User from 'double-face/lib/libs/user';
import Page from 'double-face/lib/libs/page';
import {Events} from 'backbone';
import Helmet from 'react-helmet';

class Controller {

    $layout = require('./views/layout');
    $container = 'main';
    $template = require('./templates/index.html');
    $templateDev = require('./templates/dev-index.html');
    $errorView = require('./views/error');

    get template(){
        return DEVELOPMENT ? this.$templateDev : this.$template;
    }

    constructor(req) {
        this.navigation = new Navigation(req);
        this.user = new User;
        this.global = _.extend({}, Events);
        this.page = new Page();
    }


    async exec(action, params) {
        let data = '',statusCode = 200;
        try {
            return [await this.runAction(action, params), statusCode];
        } catch (e) {
            console.log(e)
            return this.catchError(e);
        }
    }


    runAction(action, params) {
        if(this[action] === undefined){
            let error = new Error('Not Found');
            error.name = 'NotFound';
            error.statusCode = 404;
            error.req = this.navigation.req;
            throw error;
        }
        return this[action].apply(this, params);
    }

    render(view = '', layout = '') {
        layout = layout || React.createElement(this.$layout, {
                navigation: this.navigation,
                user: this.user,
                global: this.global,
                page: this.page
            }, view);
        if (SERVER) {
            let content = ReactDOMServer.renderToString(layout);
            let head = Helmet.rewind();
            return _.template(this.template)({content: content,...head})
        } else {
            ReactDOM.render(layout, document.getElementById(this.$container));
        }
    }

    catchError(e){
        let status = e.statusCode || 500,msg = e.message || 'Server Error';
        switch (e.name) {
            case 'Error':
            case 'TypeError':
                if(e.message == 'Failed to fetch'){
                    status = 504;
                    msg =  'Gateway Time-out';
                }
                break;
        }
        return [this.showError(status,msg), status];
    }

    showError(status, msg){
        let ErrorView= this.$errorView;
        return this.render(<ErrorView status={status} msg={msg} />);
    }
}


export default Controller;