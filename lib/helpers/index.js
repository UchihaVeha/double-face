

exports.attrToLabel = function(attr = ''){
    return (attr.charAt(0).toUpperCase() + attr.slice(1)).replace(/(.)([A-Z])/g,'$1 $2');
};
