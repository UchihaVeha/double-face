
var merge = require('deepmerge');
function Criteria(criteriaObj,newObj) {
    let criteria = merge({},criteriaObj);
    Methods.parseObject(criteria ,newObj);
    return criteria
}
var Methods = {
    isMethod(methods){
        return this.getMethod(methods) !== false;
    },

    getMethod(methods){
        let m = false;
        for(var method in methods){
            let str = 'method' + method.substr(1,1).toUpperCase() + method.substr(2);
            if(typeof method === 'string' && str in this){
               m = [str,methods[method]];
            }
        }
        return m;
    },

    execMethod(criteria,newObj,key){
        this[this.getMethod(newObj[key])[0]].apply(this,[criteria,this.getMethod(newObj[key])[1],key])
    },

    parseObject(criteria,newObj){
        for(var key in newObj){
            if(newObj[key] == undefined ){
                delete criteria[key]
            }else if(typeof newObj[key] === 'object' && !('length' in newObj[key]) && this.isMethod(newObj[key])){
               this.execMethod(criteria,newObj,key)
            }else{
                criteria[key] = newObj[key];
            }
        }
    },

    methodToggle(criteria,newValue,key){
        if(typeof newValue === 'string'){
            if(!(key in criteria)){
                criteria[key] = newValue;
            }else{
                if(criteria[key] == newValue){
                    delete criteria[key];
                }else{
                    criteria[key] = newValue;
                }
            }
        }else if(typeof newValue === 'object' && 'length' in newValue){
            if(!(key in criteria)){
                criteria[key] = newValue[0];
            }else{
                let isDetect = false;
                for(var i=0; i < newValue.length; i++){
                    if(newValue[i] == criteria[key]){
                        if(i == newValue.length - 1){
                            criteria[key] = newValue[0]
                        }else{
                            criteria[key] = newValue[i+1]
                        }
                        isDetect = true;
                        break;
                    }
                }
                !isDetect && (criteria[key] = newValue[0])
            }
        }else{
            console.warn('Criteria @toggle: not support this value data type');
        }
    }

};



export default Criteria;