import React from 'react';
import { Table, Button, Glyphicon, Input,Pagination } from 'react-bootstrap';
import {attrToLabel} from 'framework/Helpers';
import Link from 'framework/Widgets/Link';
import DropdownInput from 'framework/Widgets/DropdownInput';
var DropdownList = require('react-widgets').DropdownList;

/**
* Render grid from configs
* Example usage:
* let columns = [
*    {
*       attr:'project.name',
*       filter:{
*            type:'dropdownList',
*            collection:this.props.projectsCollection
*        },
*        sort:false,
*        style:{
*            minWidth:'200px'
*        }
*    },
*    'employeeName',
*    {
*        attr: 'date',
*        value: function (model, attr) {
*            return moment(model.get(attr)).format('DD-MMM-YYYY')
*        },
*    },
*    {
*        attr: 'jiraTicket',
*        value: function (model, attr) {
*            return <a href={'https://onix-systems.atlassian.net/browse/' + model.get(attr)}>{model.get(attr)}</a>
*        },
*        style:{whiteSpace:'nowrap'}
*    },
*    {
*        attr:'status',
*        filter:['','new','approved']
*    },
*    ...
*];
*
* ...
*
* <Grid responsive condensed hover {...this.props} columns={columns}/>
*/
export default  class ActiveTable extends React.Component {


    constructor(props) {
        var {collection ,navigation , ...otherProps} = props;
        super();
        this.state = {collection: collection, navigation: navigation};
        this.columns = [];
        this.prepareColumnsData(otherProps.columns)
    }

    prepareColumnsData(columns) {
        this.columns = columns.map(el => {
            if (typeof el === 'string') {
                el = {
                    attr: el,
                    sort: true
                };
            }
            if (el.value === undefined && el.attr != undefined) {
                el.value = function (model, attr) {
                    let splitAttr = attr.split('.');
                    if (splitAttr.length > 1) {
                        return (model.get(splitAttr[0]) && model.get(splitAttr[0])[splitAttr[1]]) ? model.get(splitAttr[0])[splitAttr[1]] : '';
                    } else {
                        return model.get(attr)
                    }
                }
            }
            if (el.label === undefined && el.attr != undefined) {
                let splitAttr = el.attr.split('.');
                el.label = attrToLabel(splitAttr[0]);
            }
            if (el.sort === undefined && el.attr != undefined) {
                el.sort = true;
            }
            if (el.filter === undefined && el.attr !== undefined) {
                el.filter = {type: 'text'}
            } else if (typeof el.filter === 'object' && 'length' in el.filter) {
                el.filter = {
                    type: 'select',
                    elements: el.filter
                }
            }
            return el;
        });
    }

    componentWillReceiveProps(props) {
        this.setState({collection: props.collection, navigation: props.navigation});
    }

    renderHeader() {
        return (
            <thead>
            <tr>
                {this.columns.map((el, i)=> {
                    if (el.sort === true) {
                        let sort = {sort: {'@toggle': [el.attr + ' ASC', el.attr + ' Desc']}};
                        let url = this.state.navigation.createUrl(null, sort)
                        let onClick = (e)=> {
                            e.preventDefault();
                            this.state.navigation.go(url);
                        };
                        return <th key={i} style={el.style || {}}><a href={url} onClick={onClick}>{el.label}</a></th>
                    } else {
                        return <th key={i} style={el.style || {}}>{el.label}</th>
                    }

                })}
            </tr>
            <tr>
                {this.columns.map((el, i)=> {
                    if (el.filter != undefined && el.filter !== false) {
                        return <td key={i} style={el.style || {}}>{this.renderHeaderFilter(el.filter, el.attr)}</td>
                    }else{
                        return <td key={i} style={el.style || {}}></td>
                    }
                })}
            </tr>
            </thead>
        );
    }

    renderHeaderFilter(filter, attr) {
        let component = '';
        let {type, ...props} = filter;
        let UrlParams = {};
        if(this.state.navigation.params.page !== undefined){
            UrlParams.page = 1;
        }
        switch (type) {
            case 'text':
                if (props.onChange === undefined) {
                    props.onChange = (e)=> {
                        e.preventDefault();
                        UrlParams[attr] = e.target.value || null;
                        this.state.navigation.go(this.state.navigation.createUrl(null, UrlParams));
                    }
                }
                if (props.defaultValue === undefined) {
                    props.value = this.state.navigation.params[attr] || '';
                }
                let onClickRemove = (e)=> {
                    e.preventDefault();
                    this.state.navigation.go(this.state.navigation.createUrl(null, {[attr]: null}));
                };
                let removeBtn = <Button onClick={onClickRemove}><Glyphicon glyph="remove"/></Button>;
                component = <Input type="text"  {...props} buttonAfter={removeBtn}/>
                break;
            case 'select':
                if (props.onChange === undefined) {
                    props.onChange = (e)=> {
                        e.preventDefault();
                        UrlParams[attr] = e.target.value || null;
                        this.state.navigation.go(this.state.navigation.createUrl(null, UrlParams));
                    }
                }
                if (props.defaultValue === undefined) {
                    props.defaultValue = this.state.navigation.params[attr] || '';
                }
                component = (
                    <Input type='select' style={{minWidth:'100px'}} {...props} >
                        {props.elements.map((el=> {
                            return <option key={(typeof(el)==='string') ? el : el.value}
                                           value={(typeof(el)==='string') ? el : el.value}>{(typeof(el) === 'string') ? el : el.text}</option>
                        }))}
                    </Input>
                );
                break;
            case 'dropdownInput':
                if (props.onSelectItem === undefined) {
                    props.onSelectItem = (model, e)=> {
                        UrlParams[attr] = model.get('name') || null;
                        this.state.navigation.go(this.state.navigation.createUrl(null, UrlParams));
                    }
                }
                props.value = this.state.navigation.params[attr] || '';
                let onClick = (e)=> {
                    e.preventDefault();
                    this.state.navigation.go(this.state.navigation.createUrl(null, {[attr]: null}));
                };
                let button = <Button onClick={onClick}><Glyphicon glyph="remove"/></Button>;
                component = <DropdownInput {...props} buttonAfter={button}/>;
                break;
            case 'dropdownList':
                if (props.onSelect === undefined) {
                    props.onSelect = (params)=> {
                        UrlParams[attr] = params.id || null;
                        this.state.navigation.go(this.state.navigation.createUrl(null, UrlParams));
                    }
                }
                props.textField = props.textField || 'name';
                props.defaultValue = '';
                if(this.state.navigation.params[attr] !== undefined){
                    let find = props.collection.findWhere({id:this.state.navigation.params[attr]});
                    if(find && find.get(props.textField)){
                        props.defaultValue = find.get(props.textField) || '';
                    }
                }
                component =  <DropdownList
                    defaultValue={props.defaultValue}
                    data={props.collection.toJSON()}
                    textField={props.textField}
                    caseSensitive={false}
                    minLength={props.minLength || 2}
                    onSelect={props.onSelect}
                    filter={props.filter || 'contains'}/>;
                break;
            default:
                console.warn('Table: unknown filter type')
        }

        return component;
    }

    renderBody() {
        return (
            <tbody>
            {this.state.collection.map((model, i)=> {
                return this.renderRow(model,i)
            })}
            </tbody>
        )
    }

    renderRow(model,i) {
        if(this.props.renderRow){
            let getCells = ()=> this.getCells(model);
            return this.props.renderRow.apply(this,[model,getCells,i]);
        }
        return <tr key={model.get('id')}>{this.getCells(model)}</tr>
    }

    getCells(model){
        return this.columns.map( (el, i)=> {
            let value = '';
            if (typeof el.value === 'function') {
                value = el.value.apply(this, [model, el.attr , i])
            }
            return  this.renderCell(el, model, value, i);
        });
    }

    renderCell(el, model, value, i) {
        return (<td key={i} style={el.style}>{value}</td>)
    }

    renderPagination(){
        let changePage = (e,key) =>{
            e.preventDefault();
            this.props.navigation.reload({page:key.eventKey})
        };
        let pager = this.state.collection.Find().pagination() || '';
        if(pager){
            pager =  (<Pagination
                prev={pager.prev_page}
                nex={pager.next_page}
                first
                last
                ellipsis
                items={pager.count_pages}
                maxButtons={5}
                activePage={pager.cur_page}
                onSelect={changePage}/>)
        }
        return pager;
    }

    render() {
        return (
            <div className="grid">
                <Table  {...this.props}>
                    {this.renderHeader()}
                    {this.renderBody()}
                </Table>
                {this.renderPagination()}
            </div>

        )
    }
}
