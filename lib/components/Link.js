
import React from 'react';


export default class Link extends React.Component{

    static contextTypes = {
        navigation: React.PropTypes.object
    };

    constructor(props){
        super();
        this.state = {
            url:props.url,
            urlParams:props.urlParams,
            criteria: props.criteria || false
        }
    }

    componentWillReceiveProps(props){
        this.setState({
            url:props.url,
            urlParams:props.urlParams,
            criteria: props.criteria || false
        });
    }

    click = (e)=>{
        e.preventDefault();
        this.context.navigation.to(this.state.url,this.state.urlParams,this.state.criteria);
    }

    render() {
        let {url, urlParams, ...otherProps} = this.props;
        return (
            <a href={this.context.navigation.createUrl(this.state.url,this.state.urlParams,this.state.criteria)} onClick={this.click} {...otherProps} >{this.props.children}</a>
        )
    }
}
