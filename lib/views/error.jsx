
import React from 'react';

export default  class NotFound extends React.Component{
    constructor(){
        super();
    }

    render() {
        return (
            <div className="col-lg-12">
                <div className="row">
                    <h1>{this.props.status}</h1>
                </div>
                <div className="row">
                    <h2>{this.props.msg}</h2>
                </div>
            </div>
        );
    }
}
