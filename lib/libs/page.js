var _ = require('underscore');

export default class Page {


    _title  = '';
    _meta   = [];
    _link   = []

    constructor(canonical = null){
        if(canonical){
            this.canonical = canonical;
        }
    }

    get title(){
        return this._title;
    }

    set title(str){
        return this._title = str;
    }

    set keywords(str){
        return this._meta.push({"name": "keywords", "content": str});
    }

    set description(str){
        return this._meta.push({"name": "description", "content": str});
    }

    set canonical(str){
        if(str){
            return this._link.push({"rel": "canonical", "href": str});
        }
    }

    addMeta(obj){
        if(Array.isArray(obj)){
            this._meta = obj.concat(this._meta) ;
        }else{
            this._meta.push(obj);
        }
    }

    toJSON(){
        return {
            title : this._title,
            meta : this._meta,
            link: this._link
        }
    }
}