(function() {
    'use strict';
    var _ = require('underscore');
    var Backbone = require('backbone');
    var methodMap = {
        'create': 'POST',
        'update': 'PUT',
        'patch':  'PATCH',
        'delete': 'DELETE',
        'read':   'GET'
    };

    var urlError = function(){
        throw new Error('A "url" property or function must be specified');
    };

    var sync = function(method, model, options) {
        var type = methodMap[method];

        // Default options, unless specified.
        options = options || {};

        // Default JSON-request options.
        var params = {type: type, dataType: 'json'};

        // Ensure that we have a URL.
        if (!options.url) {
            params.url = _.result(model, 'url') || urlError();
        }

        // Ensure that we have the appropriate request data.
        if (options.data == null && model && (method === 'create' || method === 'update' || method === 'patch')) {
            params.contentType = 'application/json';
            params.data = JSON.stringify(options.attrs || model.toJSON(options));
        }

        // Don't process data on a non-GET request.
        if (params.type !== 'GET') {
            params.processData = false;
        }


        // Make the request, allowing the user to override any Ajax options.
        var xhr = options.xhr = Backbone.ajax(_.extend(params, options));
        model.trigger('request', model, xhr, options);
        return xhr.catch(function(error){
            throw error
        }).then(function(response){
            if(model.afterRequest !== undefined){
                return model.afterRequest(response);
            }
            return response;
        }).catch(function(error){
            throw error;
        }).then(function(response){
            options.success(response);
            return response;
        })
    };

    if (typeof exports === 'object') {
        module.exports = sync;
    } else {
        Backbone.sync = sync;
    }
})();