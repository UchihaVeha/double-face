(function() {
    'use strict';

    var _ = require('underscore');
    var Backbone = require('backbone');
    var validation = require('backbone-validation');
    _.extend(Backbone.Model.prototype, validation.mixin);
    Backbone.ajax = require('./ajax');
    Backbone.sync  = require('./sync');

    module.exports = Backbone;
})();