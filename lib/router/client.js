import Backbone from 'backbone';
var BaseRouter = require('./base');


class Router extends BaseRouter{
    constructor(config){
        super(config);
        var router = this;
        this._app =  new (Backbone.Router.extend({
            initialize: function () {
                this.route(/(.*)/,'any',function(params){
                    if(params === null){
                        params = '';
                    }
                    params = '/' + params;
                    router.trigger(params);
                });
            }
        }));
    }


    start(){
        Backbone.history.start({pushState: true, hashChange: false});
    }
}

export default Router;